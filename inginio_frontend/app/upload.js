

window.onload = function(){
    let serviceLocation = `${window.location.protocol}//64.74.216.173:32090`;
    if(serviceLocation[serviceLocation.length - 1] !== '/'){
        serviceLocation += '/';
    }
    localStorage.setItem('service location', serviceLocation);
    let fileInputs = document.getElementsByClassName('custom-file-input');
	console.log(fileInputs);
	console.log(fileInputs[0]);
	console.log(fileInputs[1]);
    fileInputs[0].onchange = function(){
        let name = document.createTextNode('Choose file');
        if(fileInputs[0].files && fileInputs[0].files[0]){
            [{name}] = fileInputs[0].files;
        }
        document.querySelector('label[for="file1"]').innerHTML = name;
    };
	fileInputs[1].onchange = function(){
        let name = document.createTextNode('Choose file');
        if(fileInputs[1].files && fileInputs[1].files[0]){
            [{name}] = fileInputs[1].files;
        }
        document.querySelector('label[for="file2"]').innerHTML = name;
    };
    document.querySelector('#uploadform').onsubmit = function(e){
        document.getElementById("knowledge").style.display = "none";
        e.preventDefault();
        $('span.fas').css('color', '').
            removeClass('fa-times-circle').
            addClass('fa-spinner fa-spin');
        let formData = new FormData();
		let drugfiles=document.getElementsByClassName('custom-file-input');
		console.log(drugfiles[0]);
        formData.append('file', drugfiles[0].files[0]);
		formData.append('file', drugfiles[1].files[0]);
		console.log(formData);
        $.post({
            contentType: false,
            data: formData,
            error: function(){
                $('span.fas.fa-spinner.fa-spin').toggleClass('fa-spinner fa-spin fa-times-circle').
                    css('color', 'red');
            },
            processData: false,
            statusCode: {
                200: function(data){
                    $('span.fas.fa-spinner.fa-spin').removeClass('fa-spinner fa-spin');
					console.log(data);
					alert("Your files have been uploaded");
					document.querySelector('label[for="file2"]').innerHTML="";
					document.querySelector('label[for="file1"]').innerHTML="";
					document.getElementById("knowledge").style.display = "block";
                }
            },
            url: `${localStorage.getItem('service location')}initiate_pipeline`,
			beforeSend: function (xhr) {
				xhr.setRequestHeader("withCredentials",true);
			}
        });
    };
	document.querySelector('#searchPage').onclick=function(e){
		e.preventDefault();
		window.location.href= "./drugdetails.html";
	};
};
