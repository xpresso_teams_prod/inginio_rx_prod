from flask import Flask, send_from_directory, make_response
from flask_jwt import JWT
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
from ibm_watson import AssistantV2
from werkzeug.security import safe_str_cmp

authenticator = IAMAuthenticator('L1VDFuYEFsgQXVSbE5x54fDBINpZbTtvs3gHZDPdYx8y')
assistant = AssistantV2(
    version='2020-04-01',
    authenticator=authenticator
)

assistant.set_service_url(
    'https://api.us-south.assistant.watson.cloud.ibm.com/instances/26715b2a-c949-4d2b-980c-eeddc774ec91')

app = Flask(__name__, static_url_path='')
app.config['SECRET_KEY'] = 'inginio_xpresso_demo'


def response(file):
    response = make_response(send_from_directory('.', file))
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = 0
    return response


class User(object):
    def __init__(self, id, username, password):
        self.id = id
        self.username = username
        self.password = password

    def __str__(self):
        return "User(id='%s')" % self.id


users = [
    User(1, 'user1', 'abcxyz'),
    User(2, 'user2', 'abcxyz'),
]

username_table = {u.username: u for u in users}
userid_table = {u.id: u for u in users}


def authenticate(username, password):
    user = username_table.get(username, None)
    if user and safe_str_cmp(user.password.encode('utf-8'), password.encode('utf-8')):
        return user


def identity(payload):
    user_id = payload['identity']
    return userid_table.get(user_id, None)


jwt = JWT(app, authenticate, identity)


@app.route('/')
@app.route('/<file>')
# @jwt_required()
def landing(file=None):
    if file is None:
        return response('drugdetails.html')

    elif file == 'upload':
        return response('./upload.html')

    else:
        return response(file)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8080)
