#! /bin/bash

mongoimport --db rxp --collection plandocs --drop --file /data/db/rxp.plandocs.json --jsonArray
mongoimport --db rxp --collection druglists --drop --file /data/db/rxp.druglists.json --jsonArray
mongoimport --db rxp --collection druglists --drop --file /data/db/ndc.json --jsonArray