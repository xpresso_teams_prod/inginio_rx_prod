"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
__author__ = "Naveen Sinha"

import json
import logging
import os

from config import rx_config
from flask import Flask
from flask import request, jsonify
from flask_cors import CORS, cross_origin
from pymongo import MongoClient
from run_pipeline import Submit
from werkzeug.utils import secure_filename
from xpresso.ai.core.logging.xpr_log import XprLogger

config_file = 'config/dev.json'

logger = XprLogger("rx_xpresso_service", level=logging.INFO)


def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__, template_folder='../templates')
    return flask_app


app = create_app()
CORS(app)


@app.route("/getDrugDetails", methods=['GET', "POST"])
@cross_origin(origin='*')
def home_page():
    data = json.loads(request.data)
    drugName = data['drugname']
    conn = MongoClient(rx_config.MONGO_KEY)
    plancoll = conn.rxp.druglists
    query = {"drugname": {"$regex": f'{drugName}.*', "$options": 'i'}}
    docs = list(plancoll.find(query, {"_id": 0, 'drugname': 1, "ndc": 1}))
    return jsonify(docs)


@app.route('/initiate_pipeline', methods=["POST"])
@cross_origin(origin='*')
def initiate_pipeline():
    """
        Service to start pipeline on xpresso
    """
    if request.method == "POST":
        f = request.files['file']
        if not os.path.exists(os.path.join(rx_config.MOUNT_PATH, rx_config.CONTENT_FOLDER)):
            os.makedirs(os.path.join(rx_config.MOUNT_PATH, rx_config.CONTENT_FOLDER))

        f.save(os.path.join(rx_config.MOUNT_PATH, rx_config.CONTENT_FOLDER,
                            secure_filename(f.filename)))
        submit_obj = Submit(secure_filename(f.filename))
        submit_obj.submit()

    return jsonify({'success': True}), 200


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)
