# Paths variable
MOUNT_PATH = "/data"
PDF_FOLDER = "content"


# Details of insurer and plans
INSURERS = ["Express Scripts", "Humana"]
ES_PLAN_ID_TERMS = ["PPO", "H.S.A", "P.P.O"]
HUMANA_PLAN_ID_TERMS = ["PDP"]
